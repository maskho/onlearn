/*
 * BottomNavigation.tsx
 * BottomNavigation component.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *
 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

import { Button } from "@material-ui/core";
import React from "react";
import { useStyles } from "./styles";
import { BrowserRouter } from "react-router-dom";

const BottomNavigation: React.FC = () => {
  const classes = useStyles();
  return (
    <BrowserRouter>
      <div className={classes.navButtons}>
        <Button variant="contained" color="primary" href="/">
          Home
        </Button>
        <Button variant="contained" color="primary" href="/#/Page2">
          Page 2
        </Button>
        <Button variant="contained" color="primary" href="/#/Page3">
          Page 3
        </Button>
      </div>
    </BrowserRouter>
  );
};

export default BottomNavigation;
