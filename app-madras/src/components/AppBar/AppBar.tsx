/*
 * AppBar.tsx
 * AppBar component.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *
 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

import { AppBar as AppBarMUI, Toolbar } from "@material-ui/core";
import React from "react";
import { useStyles } from "./styles";

const AppBar: React.FC = () => {
  const classes = useStyles();

  return (
    <AppBarMUI className={classes.appBar} position="fixed">
      <Toolbar />
    </AppBarMUI>
  );
};

export default AppBar;
