/*
 * index.ts
 * Containers import/export.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *

 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

export { default as App } from "./App/App";
export { default as Home } from "./Home/Home";
export { default as Routes } from "./Routes/Routes";
export { default as Page2 } from "./Page2/Page2";
export { default as Page3 } from "./Page3/Page3";
