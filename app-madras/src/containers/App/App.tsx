/*
 * App.tsx
 * App container.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *

 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

import { CssBaseline, ThemeProvider } from "@material-ui/core";
import { Layout } from "../../components";
import { Routes } from "../../containers";
import React from "react";
import { Provider } from "react-redux";
import { store } from "../../redux/store";
import "typeface-roboto";
import { theme } from "./theme";

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Layout>
          <Routes />
        </Layout>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
