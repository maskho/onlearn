/*
 * index.ts
 * Index reducers.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *

 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

import { INCREMENT } from "../actions/constants";

export const counter = (
  state: { count: number } = { count: 0 },
  action: { payload: any; type: string }
) => {
  switch (action.type) {
    case INCREMENT:
      return action.payload;
    default:
      return state;
  }
};
