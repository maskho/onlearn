/*
 * Routes.tsx
 * Routes component.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *

 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

import { PageNotFound } from "../../components";
import { Page2 } from "../../containers";
import { Page3 } from "../../containers";
import { Home } from "../../containers";
import React from "react";
import { HashRouter, Route, Switch } from "react-router-dom";

const Routes: React.FC = () => (
  <HashRouter>
    <Switch>
      <Route component={Home} exact={true} path="/" />
      <Route component={Page2} exact={true} path="/Page2" />
      <Route component={Page3} exact={true} path="/Page3" />
      <Route component={PageNotFound} />
    </Switch>
  </HashRouter>
);

export default Routes;
