/*
 * styles.ts
 * BottomNavigation component jss styles.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *
 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

import { createStyles, makeStyles, Theme } from "@material-ui/core";

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    bottomNavigation: {
      bottom: 0,
      overflow: "hidden",
      position: "fixed",
      width: "100%",
    },
    navButtons: {
      textAlign: "center",
      position: "fixed",
      bottom: "0",
      left: "0",
      width: "100%",
    },
  })
);
