/*
 * Layout.tsx
 * Layout component.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *
 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

import { Container } from "@material-ui/core";
import React from "react";
import AppBar from "../AppBar/AppBar";
import BottomNavigation from "../BottomNavigation/BottomNavigation";
import { useStyles } from "./styles";

interface IProps {
  children: React.ReactNode;
}

const Layout: React.FC<IProps> = (props) => {
  const classes = useStyles();

  return (
    <>
      <AppBar />
      <Container classes={{ root: classes.containerRoot }}>
        <main className={classes.root}>{props.children}</main>
      </Container>
      <BottomNavigation />
    </>
  );
};

export default Layout;
