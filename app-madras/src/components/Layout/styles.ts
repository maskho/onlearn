/*
 * styles.ts
 * Layout component jss styles.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *
 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

import { createStyles, makeStyles, Theme } from "@material-ui/core";

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    [theme.breakpoints.only("xs")]: {
      containerRoot: {
        paddingBottom: `${theme.spacing(2)}px`,
        paddingTop: `${theme.spacing(2)}px`,
      },
      root: {},
    },
    [theme.breakpoints.only("sm")]: {
      containerRoot: {
        paddingBottom: `${theme.spacing(3)}px`,
        paddingTop: `${theme.spacing(3)}px`,
      },
      root: {},
    },
    [theme.breakpoints.up("md")]: {
      containerRoot: {
        paddingBottom: `${theme.spacing(4)}px`,
        paddingTop: `${theme.spacing(4)}px`,
      },
      root: {},
    },
    containerRoot: {
      marginBottom: 56,
      marginTop: 64,
    },
  })
);
