/*
 * Home.tsx
 * Home container.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *
 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

import { Button, Typography } from "@material-ui/core";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { incrementCount } from "../../redux/actions";

const Home: React.FC = () => {
  const count = useSelector((state: any) => state.counter.count);
  const dispatch = useDispatch();

  const handleIncrementClick = (event: React.ChangeEvent<{}>) => {
    dispatch(incrementCount(count));
  };

  return (
    <>
      <Typography variant="h3">PWA-Starter</Typography>
      <Typography gutterBottom={true}>
        This application is a react starter application used by DHA developers
        for developing progressive web applications.
      </Typography>
      <Typography gutterBottom={true}>
        This project was bootstrapped with Create React App.
      </Typography>
      <Typography variant="h4">Getting Started</Typography>
      <ul>
        <li>git clone git@bitbucket.org:wmtp/pwa-starter.git</li>
        <li>cd pwa-starter</li>
        <li>npm i</li>
        <li>npm start</li>
      </ul>
      <Typography variant="h4">New Project Instructions</Typography>
      <ul>
        <li>.gitignore</li>
        <li>
          Uncomment build folder if its not needed in the repository for CI/CD
          or hosting.
        </li>
        <li>package.json</li>
        <li>
          Add or replace the values for: name, version, author, description,
          repository.url, and bug.url.
        </li>
        <li>Copyright Headers</li>
        <li>Add copyright headers to every new file.</li>
        <li>Add the file name on the first line.</li>
        <li>Add the file description on the second line</li>
        <li>
          Replace Syeh A., Khobar, and date for the line "Created by Syeh A.
          Khobar on a night when he thinks about the future".
        </li>
        <li>
          Search and replace all instances of "pwa-starter" with your
          application name in all project files.
        </li>
      </ul>
      <Typography variant="h4">Example of React Component</Typography>
      <Typography gutterBottom={true}>Redux Counter: {count}</Typography>
      <Button
        color="primary"
        onClick={handleIncrementClick}
        variant="contained"
      >
        Increment
      </Button>
    </>
  );
};

export default Home;
