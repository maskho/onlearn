/*
 * PageNotFound.tsx
 * PageNotFound component.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *

 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

import { Typography } from "@material-ui/core";
import React from "react";
import { useStyles } from "./styles";

const Page3: React.FC = () => {
  const classes = useStyles();

  return <Typography className={classes.Page2}>This is Page 3</Typography>;
};

export default Page3;
