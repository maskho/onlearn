/*
 * App.test.tsx
 * App component testing.
 *
 * Created by Syeh A. Khobar on a night when he thinks about the future.
 *
 * MadrasApp
 * email: madras.app@protonmail.com
 *

 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

import { render } from "@testing-library/react";
import React from "react";
import App from "./App";

test("renders redux counter", () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Redux Counter: 0/i);
  expect(linkElement).toBeInTheDocument();
});
